<?php

use Modules\User\Models\Permission;
use Modules\User\Models\Role;

return [
    'name' => 'User',
    'morph_map' => [
        'role' => Role::class,
        'permission' => Permission::class
    ],
    'menu' => [
        'admin' => [
            'user' => [
                'position' => 4,
                'route' => 'admin.user.index',
                'icon' => 'user',
                'label' => 'Pengguna',
                'children' => [
                    [
                        'route' => 'admin.user.index',
                        'label' => 'Pengguna',
                    ],
                    [
                        'route' => 'admin.user.role',
                        'label' => 'Peran',
                    ],
                    [
                        'route' => 'admin.user.permission',
                        'label' => 'Hak Akses',
                    ],
                ],
            ],
        ],
    ],
];
