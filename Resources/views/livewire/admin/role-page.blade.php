@section('page-title', 'Peran')

<div x-data="rolePage">

    <x-ui::layout.list-with-detail title="Peran" selected-var="selectedRole">
        <x-slot name="afterTitle">
            <x-ui::button.secondary
                x-on:click="newData"
                x-show="!selectedRole"
                class="!ml-auto sm:!ml-4"
            >Baru</x-ui::button.secondary>
        </x-slot>

        <x-slot name="detailTitle">
            <span x-text="selectedRole == 'new' ? 'Peran Baru' : 'Detil Peran'"></span>
        </x-slot>

        <x-slot name="list">
            <x-ui::select-list.generic
                id="role-select-list"
                data-hub="User::GetPaginatedRoles"
                x-model="selectedRole"
                x-on:change="input = $event.detail ? prepareForEdit($event.detail) : {}"
                height="calc(100vh - 140px)"
            />
        </x-slot>

        <x-slot name="detail">
            <div class="px-4 bg-white">

                <div class="space-y-6 sm:space-y-5">

                    <div class="text-sm sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                        <span class="block font-medium text-gray-700 sm:mt-px sm:pt-1">
                            Nama Peran
                        </span>
                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                            <x-ui::input
                                x-model="input.name" class="w-full max-w-lg sm:max-w-xs"
                                x-bind:class="{
                                    '!border-red-500 focus:!ring-red-500': getError('input.name')
                                }"
                                x-ref="firstInput"
                            />
                            <p x-text="getError('input.name')" class="mt-1 text-xs text-red-500"></p>
                        </div>
                    </div>

                    <div class="text-sm sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5">
                        <span class="block font-medium text-gray-700 sm:mt-px sm:pt-1">
                            Hak Akses
                        </span>
                        <div class="mt-1 sm:mt-0 sm:col-span-2">
                            <x-ui::input.select x-model="input.permissions" class="w-full max-w-lg sm:max-w-xs" multiple="multiple">
                                <template x-for="(permission, key) in dataPermissions">
                                <option x-bind:value="key" x-text="permission"></option>
                                </template>
                            </x-ui::input.select>

                            <div class="mt-4 space-y-2">
                                <x-ui::button.secondary
                                    x-show="newPermission === null"
                                    x-on:click="newPermission = ''; $nextTick(() => setTimeout(() => $refs.inputPermission.focus(), 100))"
                                    x-ref="buttonNewPermission"
                                >Tambah Hak Akses</x-ui::button.secondary>

                                <input
                                    x-show="newPermission !== null"
                                    x-model="newPermission"
                                    x-ref="inputPermission"
                                    x-on:keydown.escape="cancelPermission"
                                    x-on:keydown.enter.prevent="savePermission"
                                    x-bind:class="{
                                        '!border-red-500 focus:!ring-red-500': getError('input.permission')
                                    }"
                                    type="text"
                                    class="block w-full max-w-lg py-1 text-sm border-gray-300 rounded-md shadow-sm focus:ring-green-500 focus:border-green-500 sm:max-w-xs"
                                    placeholder="Masukkan nama akses"
                                >
                                <p x-text="getError('input.permission')" class="mt-1 text-xs text-red-500"></p>
                                <div x-show="newPermission !== null" class="flex space-x-2">
                                    <x-ui::button.primary
                                        type="button"
                                        x-on:click.prevent="savePermission"
                                        x-bind:disabled="newPermission === ''"
                                    >Simpan Hak Akses</x-ui::button.primary>
                                    <x-ui::button.secondary x-on:click="cancelPermission">Batal</x-ui::button.secondary>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <pre x-json="input"></pre> --}}

                </div>

            </div>
        </x-slot> <!-- name=detail -->

        <x-slot name="detailActions">
            <x-ui::button.primary x-on:click="storeData">Simpan</x-ui::button.primary>

            <x-ui::button.secondary
                x-show="input.id"
                x-on:click="openConfirm('Mau menghapus pengguna: ' + input.name + '?').then(async () => {
                    await $wire.delete(input.id)
                    xData('#select-list').refreshData()
                })"
                class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
            >Hapus</x-ui::button.secondary>
        </x-slot>
    </x-ui::layout.list-with-detail>

    <script>
        function rolePage() {
            return {
                selectedRole: null,
                input: {},
                errors: {},

                dataPermissions: [],
                async init() {
                    this.loadPermissions()
                },

                async loadPermissions() {
                    this.dataPermissions = await dataHub('User::GetAllPermissions', {asKeyValue: 1})
                },

                newData() {
                    this.selectedRole = 'new'
                    this.input = {}

                    setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
                },
                prepareForEdit(input) {
                    if (typeof input != 'object') return input

                    errors = {}

                    // {{-- map related data to id only --}}
                    input.permissions = input.permissions.map(u => u.id)

                    // {{-- delete uneditable attributes --}}
                    delete input.created_at
                    delete input.updated_at

                    return input
                },
                storeData() {
                    this.$wire.store(this.input).then((resp) => {
                        if (!resp || resp.failed > 0) {
                            this.errors = livewireErrors('{{ $this->id }}')
                            return;
                        }
                        this.errors = {}
                        xData('#role-select-list').refreshData(resp)
                    })
                },
                getError(fieldName) {
                    if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                        return this.errors[fieldName][0]
                    }

                    return null
                },

                newPermission: null,
                async savePermission() {
                    if (this.newPermission === '') return

                    const resp = await this.$wire.storePermission(this.newPermission)

                    if (!resp || resp.failed > 0) {
                        this.errors = livewireErrors('{{ $this->id }}')
                        return;
                    }

                    if (this.getError('input.permission')) delete this.errors['input.permission']

                    this.loadPermissions()

                    if (!Array.isArray(this.input.permissions))
                        this.input.permissions = []

                    this.input.permissions.push(resp.id)
                    this.newPermission = null
                    this.$nextTick(() => setTimeout(() => this.$refs.buttonNewPermission.focus(), 100))
                },
                cancelPermission() {
                    this.newPermission = null; this.$nextTick(() => setTimeout(() => this.$refs.buttonNewPermission.focus(), 100))

                    if (this.getError('input.permission')) delete this.errors['input.permission']
                },
            }
        }
    </script>
</div>
