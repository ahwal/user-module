@section('page-title', 'Hak Akses')

<div x-data="dataPage">

    <x-ui::layout.list-with-detail title="Hak Akses" selected-var="selectedData">
        <x-slot name="afterTitle">
            <x-ui::button.secondary
                x-on:click="newData"
                x-show="!selectedData"
                class="!ml-auto sm:!ml-4"
            >Baru</x-ui::button.secondary>
        </x-slot>

        <x-slot name="detailTitle">
            <span x-text="selectedData == 'new' ? 'Hak Akses Baru' : 'Detil Hak Akses'"></span>
        </x-slot>

        <x-slot name="list">
            <x-ui::select-list.generic
                id="data-select-list"
                data-hub="User::GetPaginatedPermissions"
                x-model="selectedData"
                x-on:change="input = $event.detail ? prepareForEdit($event.detail) : {}"
                height="calc(100vh - 140px)"
            >
                <div x-text="item.data.name" class="px-1"></div>
            </x-ui::select-list.generic>
        </x-slot>

        <x-slot name="detail">
            <div class="px-4 bg-white">

                <x-ui::field-group label="Nama Hak Akses">
                    <x-ui::field-group.input
                        class="w-full max-w-lg sm:max-w-xs"
                        x-model="input.name"
                        x-ref="firstInput"
                        error="getError('input.name')"
                        required
                    />
                </x-ui::field-group>

            </div>
        </x-slot> <!-- name=detail -->

        <x-slot name="detailActions">
            <x-ui::button.primary x-on:click="storeData">Simpan</x-ui::button.primary>

            <x-ui::button.secondary
                x-show="input.id"
                x-on:click="confirmAndDelete"
                class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
            >Hapus</x-ui::button.secondary>
        </x-slot>
    </x-ui::layout.list-with-detail>

    <script>
        function dataPage() {
            return {
                selectedData: null,
                input: {},
                errors: {},

                newData() {
                    this.selectedData = 'new'
                    this.input = {}

                    setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
                },
                prepareForEdit(input) {
                    if (typeof input != 'object') return input

                    errors = {}

                    // {{-- delete uneditable attributes --}}
                    delete input.created_at
                    delete input.updated_at

                    return input
                },
                storeData() {
                    this.$wire.store(this.input).then((resp) => {
                        if (!resp || resp.failed > 0) {
                            this.errors = livewireErrors('{{ $this->id }}')
                            return;
                        }
                        this.errors = {}
                        xData('#data-select-list').refreshData(resp)
                    })
                },
                confirmAndDelete() {
                    openConfirm('Mau menghapus data ini?').then(async () => {
                        await this.$wire.delete(this.input.id)
                        xData('#data-select-list').refreshData()
                    })
                },
                getError(fieldName) {
                    if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                        return this.errors[fieldName][0]
                    }
                    return null
                },
            }
        }
    </script>
</div>
