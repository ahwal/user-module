@section('page-title', 'Pengguna')

<x-ui::layout.list-with-detail x-data="userPage" title="Pengguna" selected-var="selectedUser">
    <x-slot name="afterTitle">
        <x-ui::button.secondary
            x-on:click="newData"
            {{-- x-show="!selectedUser" --}}
            class="!ml-auto sm:!ml-4"
        >Baru</x-ui::button.secondary>
    </x-slot>

    <x-slot name="detailTitle">
        <span x-text="selectedUser == 'new' ? 'Pengguna Baru' : 'Detil Pengguna'"></span>
    </x-slot>

    <x-slot name="list">
        <x-user::user-select-list
            id="select-list"
            x-model="selectedUser"
            x-on:change="input = $event.detail ? prepareForEdit($event.detail) : {}"
            data-relations="roles"
            height="calc(100vh - 140px)"
        />
    </x-slot>

    <x-slot name="detail">
        <div class="px-4 bg-white">
            <form class="space-y-6 sm:space-y-5" x-on:submit.prevent="storeData">
                <button type="submit" x-ref="submit" hidden></button>

                <x-ui::field-group label="Username">
                    <x-ui::field-group.input
                        class="w-full max-w-lg sm:max-w-xs"
                        x-model="input.username"
                        x-ref="firstInput"
                        error="getError('input.username')"
                        required
                    />
                </x-ui::field-group>

                <x-ui::field-group label="Email">
                    <x-ui::field-group.input
                        type="email"
                        class="w-full max-w-lg sm:max-w-xs"
                        x-model="input.email"
                        error="getError('input.email')"
                        required
                    />
                </x-ui::field-group>

                <x-ui::field-group label="Password">
                    <x-ui::field-group.input
                        type="password"
                        class="w-full max-w-lg sm:max-w-xs"
                        x-model="input.password"
                        error="getError('input.password')"
                    />
                </x-ui::field-group>

                <x-ui::field-group label="Peran">
                    <x-ui::input.select x-model="input.roles" class="w-full max-w-lg sm:max-w-xs" multiple="multiple">
                        <template x-for="(role, key) in dataRoles">
                            <option x-bind:value="key" x-text="role" />
                        </template>
                    </x-ui::input.select>
                </x-ui::field-group>

            </form>

        </div>
    </x-slot> <!-- name=detail -->

    <x-slot name="detailActions">
        <x-ui::button.primary x-on:click="$refs.submit.click()">Simpan</x-ui::button.primary>

        <x-ui::button.secondary
            x-show="input.id"
            x-on:click="openConfirm('Mau menghapus pengguna: ' + input.name + '?').then(async () => {
                await $wire.delete(input.id)
                xData('#select-list').refreshData()
            })"
            class="!border-red-500 !text-red-500 hover:bg-red-100 ml-auto"
        >Hapus</x-ui::button.secondary>
    </x-slot>
</x-ui::layout.list-with-detail>

@push('scripts')
<script>
    function userPage() {
        return {
            selectedUser: null,
            input: {},
            dataRoles: [],
            errors: {},

            async init() {
                this.dataRoles = await dataHub('User::GetAllRoles', {asKeyValue: 1})

                if (this.selectedUser) {
                    // todo: load detail
                }
            },

            newData() {
                this.selectedUser = 'new'
                this.input = {}

                setTimeout(() => this.$nextTick(() => this.$refs.firstInput.focus()), 100)
            },
            prepareForEdit(input) {
                if (typeof input != 'object') return input

                this.errors = {}

                {{-- map related data to id only --}}
                input.roles = input.roles.map(u => u.id)

                {{-- delete uneditable attributes --}}
                delete input.created_at
                delete input.updated_at
                delete input.email_verified_at

                return input
            },
            storeData() {
                const action = this.input.id ? 'update' : 'store'

                this.$wire[action](this.input).then((resp) => {
                    if (!resp || resp.failed > 0) {
                        this.errors = livewireErrors('{{ $this->id }}')
                        return;
                    }
                    this.errors = {}
                    xData('#select-list').refreshData(resp)
                })
            },
            getError(fieldName) {
                if (this.errors[fieldName] && Array.isArray(this.errors[fieldName])) {
                    return this.errors[fieldName][0]
                }

                return null
            },
        }
    }
</script>
@endpush
