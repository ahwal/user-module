@props(['xModel', 'height' => '300px', 'dataRelations' => 'roles'])

<x-ui::select-list
    data-key="id"
    data-hub="User::GetPaginatedUsers"
    data-relations="{{ $dataRelations }}"

    height="{{ $height }}"

    with-search="1"
    with-filter="1"
    with-order="0"

    x-model="{{ $xModel }}"

    {{ $attributes }}
>

    <x-slot name="filter">
        <h3>Filter</h3>

        <x-ui::select.with-data
            x-model="filter.role"
            data-hub="User::GetAllRoles"
            data-key="id"
            label="Pilih Peran"
            {{ $attributes }}
        >
            <p
                class="px-2 py-1"
                x-html="item.name"
            ></p>
        </x-ui::select.with-data>

    </x-slot>


    <x-slot name="order">
        <h3>Urutkan berdasarkan</h3>
    </x-slot>

    <x-slot name="listing"> {{-- item = {index: ..., data: {...}} --}}
        <div class="flex justify-between p-1 text-sm cursor-pointer hover:bg-gray-100 focus:bg-gray-100 group"
                x-bind:class="{'!bg-blue-100': isSelected(item.data)}">
            <div class="flex items-center w-4">
                <x-ui::svg icon="check" class="w-4 h-4 opacity-0"
                    x-bind:class="{'opacity-100': isSelected(item.data)}" />
            </div>
            <div class="w-full px-2">
                <div x-text="item.data.username"></div>
                <div class="text-gray-500" x-text="item.data.email"></div>
            </div>
            <div class="flex items-center px-2 text-xs text-gray-400">
                <div x-text="item.data.roles.length ? item.data.roles[0].name : '-'"></div>
            </div>
        </div>
    </x-slot>

</x-ui::select-list>
