<?php

use Illuminate\Support\Facades\Route;
use Modules\User\Http\Livewire\Admin\PermissionPage;
use Modules\User\Http\Livewire\Admin\RolePage;
use Modules\User\Http\Livewire\Admin\UserPage;

Route::middleware(['auth:sanctum', 'verified', 'module:user'])
    ->prefix('admin/user')
    ->as('admin.user.')
    ->group(function () {

        Route::get('/', UserPage::class)
            ->name('index');
        Route::get('/role', RolePage::class)
            ->name('role');
        Route::get('/permission', PermissionPage::class)
            ->name('permission');
    });
