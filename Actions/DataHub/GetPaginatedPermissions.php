<?php
namespace Modules\User\Actions\DataHub;

use Modules\User\Models\Permission;

class GetPaginatedPermissions
{
    public static function run($params = [])
    {
        $page = $params['page'] ?? 1;
        return Permission::query()->paginate(30, ['*'], 'page', $page)->toArray();
    }
}
