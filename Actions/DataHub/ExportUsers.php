<?php
namespace Modules\User\Actions\DataHub;

use Modules\Staff\Actions\DownloadExportedStaffs;
use Modules\Staff\Models\Staff;

class ExportUsers
{
    public static function run($params = [])
    {
        // TODO: fix this, get real
        return DownloadExportedStaffs::run(
            Staff::query()
                ->filter($params['filter'] ?? [])
                ->with(['profile']),
            $params['columns'] ?? []
        );
    }
}
