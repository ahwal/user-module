<?php
namespace Modules\User\Actions\DataHub;

use Modules\User\Models\Role;

class GetAllRoles
{
    public static function run($params = [])
    {
        if (isset($params['asKeyValue'])) {
            return Role::pluck('name', 'id')->toArray();
        }

        $columns = explode(',', $params['columns'] ?? null);
        if (empty($columns[0])) $columns = ['id', 'name'];

        return Role::get($columns)->toArray();
    }
}
