<?php
namespace Modules\User\Actions\DataHub;

use Modules\User\Models\Permission;

class GetAllPermissions
{
    public static function run($params = [])
    {
        if (isset($params['asKeyValue'])) {
            return Permission::pluck('name', 'id')->toArray();
        }

        $columns = explode(',', $params['columns'] ?? null);
        if (empty($columns[0])) $columns = ['id', 'name'];

        return Permission::get($columns)->toArray();
    }
}
