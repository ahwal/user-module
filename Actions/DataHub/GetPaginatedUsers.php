<?php
namespace Modules\User\Actions\DataHub;

use App\Models\User;

class GetPaginatedUsers
{
    public static function run($params = [])
    {
        $page = $params['page'] ?? 1;

        $query = User::query();

        if (isset($params['filter'])) {
            $query->filter($params['filter']);
        }

        if (isset($params['relations']) && !empty($params['relations'])) {
            $query->with(explode(';', $params['relations']));
        }

        return $query->orderByDesc('created_at')
            ->paginate(30, ['*'], 'page', $page)->toArray();
    }
}
