<?php
namespace Modules\User\Actions\DataHub;

use Modules\User\Models\Role;

class GetPaginatedRoles
{
    public static function run($params = [])
    {
        $page = $params['page'] ?? 1;
        return Role::query()->with(['permissions'])
            ->paginate(30, ['*'], 'page', $page)->toArray();
    }
}
