<?php

namespace Modules\User\Http\Livewire\Admin;

use Livewire\Component;
use Modules\User\Models\Permission;

class PermissionPage extends Component
{
    public $input;

    public function render()
    {
        return view('user::livewire.admin.permission-page');
    }

    public function store($input)
    {
        $this->input = $input;
        // TODO: validate duplicate entry
        $validated = $this->validate([
            'input.name' => ['required'],
        ])['input'];
        $this->input = null;

        if (isset($input['id'])) {
            $role = Permission::findOrFail($input['id']);
            $role->update($validated);
        } else {
            $validated['guard_name'] = 'web';
            $role = Permission::create($validated);
        }

        $this->notify('Data berhasil disimpan');

        return $role->toArray();
    }

    public function delete(Permission $permission)
    {
        $permission->delete();

        $this->notify('Data berhasil dihapus');
    }
}
