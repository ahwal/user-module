<?php

namespace Modules\User\Http\Livewire\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Rules\Password;
use Livewire\Component;

class UserPage extends Component
{
    // this required for validation to work
    // without this, there's no error message
    public $input;

    public function render()
    {
        return view('user::livewire.admin.user-page');
    }

    public function store($input)
    {
        $validationRules = [
            'input.username' => ['required', 'min:3', 'max:255', 'unique:users,username'],
            'input.email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'input.password' => ['required', 'string', new Password],
            'input.roles' => ['array'],
            'input.roles.*' => [
                'numeric',
                'exists:' . config('permission.table_names.roles', 'roles') . ',id'
            ],
        ];

        $this->input = $input;
        $validated = $this->validate($validationRules)['input'];
        $this->input = null;

        $validated['password'] = Hash::make($validated['password']);

        $user = User::create($validated);

        if (isset($input['roles']))
            $user->syncRoles($input['roles']);

        $this->notify('Data berhasil disimpan');

        return $user->toArray();
    }

    public function update($input)
    {
        $validationRules = [
            'input.username' => [
                'required', 'min:3', 'max:255',
                Rule::unique('users', 'username')
                    ->ignore($input['id'])
            ],
            'input.email' => [
                'required', 'email', 'max:255',
                Rule::unique('users', 'email')
                    ->ignore($input['id'])
            ],
            'input.password' => ['string', new Password],
            'input.roles' => ['array'],
            'input.roles.*' => [
                'numeric',
                'exists:' . config('permission.table_names.roles', 'roles') . ',id'
            ],
        ];

        $this->input = $input;
        $validated = $this->validate($validationRules)['input'];
        $this->input = null;

        if (isset($validated['password'])) $validated['password'] = Hash::make($validated['password']);

        $user = User::findOrFail($input['id']);
        $user->update($validated);

        if (isset($input['roles']))
            $user->roles()->sync($input['roles']);

        $this->notify('Data berhasil diperbaru');

        return $user->toArray();
    }

    public function delete(User $role)
    {
        $role->delete();

        $this->notify('Data berhasil dihapus');
    }
}
