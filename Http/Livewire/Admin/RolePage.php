<?php

namespace Modules\User\Http\Livewire\Admin;

use Livewire\Component;
use Modules\User\Models\Permission;
use Modules\User\Models\Role;

class RolePage extends Component
{
    public $input;

    public function render()
    {
        return view('user::livewire.admin.role-page');
    }

    public function store($input)
    {
        // dd($input);
        $validationRules = [
            'input.name' => ['required', 'string'],
            'input.permissions' => ['array'],
            'input.permissions.*' => [
                'numeric',
                'exists:' . config('permission.table_names.permissions', 'permissions') . ',id'
            ]
        ];

        $this->input = $input;
        $validated = $this->validate($validationRules)['input'];
        $this->input = null;

        $validated['guard_name'] = 'web';

        if (isset($input['id'])) {
            $role = Role::findOrFail($input['id']);
            $role->update($validated);
        } else {
            $role = Role::create($validated);
        }

        if (isset($input['permissions']))
            $role->permissions()->sync($input['permissions']);

        $this->notify('Data berhasil disimpan');

        return $role->toArray();
    }

    public function delete(Role $role)
    {
        $role->delete();

        $this->notify('Data berhasil dihapus');
    }

    public function storePermission($name)
    {
        $this->input = ['permission' => $name];
        $this->validate(['input.permission' => ['required', 'string', 'min:3']]);
        $this->input = null;

        return Permission::create(['name' => $name, 'guard_name' => 'web'])->toArray();
    }
}
